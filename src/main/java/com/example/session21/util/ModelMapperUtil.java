package com.example.session21.util;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperUtil {
    public ModelMapper ModelMapperInit(){
        return new ModelMapper();
    }
}
