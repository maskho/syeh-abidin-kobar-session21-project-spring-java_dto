package com.example.session21.repository;

import com.example.session21.model.Penjualan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PenjualanRepo extends JpaRepository<Penjualan,Long> {
    List<Penjualan> findByBarang(String barang);
}
