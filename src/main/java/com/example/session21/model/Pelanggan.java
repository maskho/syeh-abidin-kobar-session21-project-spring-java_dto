package com.example.session21.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "pelanggan")
@Data
public class Pelanggan {
    @Id
    @GeneratedValue(generator = "generator_pelanggan")
    @SequenceGenerator(
            name = "generator_pelanggan",
            sequenceName = "sequence_pelanggan",
            initialValue=5000
    )
    private Long id;

    @NotBlank
    @Size(min = 3,max=20)
    private String nama;

    @Column(columnDefinition = "varchar(255)")
    private String alamat;
}
