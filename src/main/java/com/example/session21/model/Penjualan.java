package com.example.session21.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Date;

@Entity
@Table(name = "penjualan")
@Data
public class Penjualan {
    @Id
    @GeneratedValue(generator = "generator_penjualan")
    @SequenceGenerator(
            name="generator_penjualan",
            sequenceName = "sequence_penjualan",
            initialValue = 4000
    )
    private Long id;

    @NotBlank
    @Size(min = 3,max = 20)
    private String barang;

    @Column(columnDefinition = "varchar(255)")
    private String pelanggan;

    @Column(columnDefinition = "date default current_date")
    private Date tanggal;

    @Column(columnDefinition = "integer")
    private Integer nilai;
}
