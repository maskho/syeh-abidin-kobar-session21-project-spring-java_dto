package com.example.session21.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name="barang")
@Data
public class Barang {
    @Id @GeneratedValue(generator = "generator_barang")
    @SequenceGenerator(
            name = "generator_barang",
            sequenceName = "sequence_barang",
            initialValue = 3000
    )
    private Long id;

    @NotBlank
    @Size(min = 3,max = 20)
    private String nama;

    @Column(columnDefinition = "varchar(255)")
    private String deskripsi;
    private String asal;

    @Column(columnDefinition = "integer default 500000")
    private Integer harga;
}
