package com.example.session21.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "alamat")
@Data
public class Alamat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "provinsi")
    private String provinsi;

    @OneToOne(mappedBy = "provinsi")
    private Pelanggan pelanggan;
}
