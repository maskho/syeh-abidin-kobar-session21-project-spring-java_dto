package com.example.session21.controller;

import com.example.session21.exception.DataNotFoundException;
import com.example.session21.model.Penjualan;
import com.example.session21.repository.PenjualanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PenjualanController {
    @Autowired
    private PenjualanRepo penjualanRepo;

    @GetMapping("/penjualan/all")
    public List<Penjualan> getPenjualan(){
        return penjualanRepo.findAll();
    }
    @GetMapping("/penjualan")
    public List<Penjualan> getPenjualan(String barang){
        if (penjualanRepo.findByBarang(barang).isEmpty()){
            throw new DataNotFoundException(barang);
        }
        return penjualanRepo.findByBarang(barang);
    }
    @PostMapping("/penjualan")
    public Penjualan newPenjualan(@Valid @RequestBody Penjualan penjualan){
        return penjualanRepo.save(penjualan);
    }
    @PutMapping("/penjualan/{id}")
    public Penjualan editPenjualan(@PathVariable Long id,
                                   @Valid @RequestBody Penjualan penjualanReq){
        return penjualanRepo.findById(id)
                .map(penjualan -> {
                    penjualan.setBarang(penjualanReq.getBarang());
                    penjualan.setPelanggan(penjualanReq.getPelanggan());
                    penjualan.setTanggal(penjualanReq.getTanggal());
                    penjualan.setNilai(penjualanReq.getNilai());
                    return penjualanRepo.save(penjualan);
                }).orElseThrow(()->new DataNotFoundException(id));
    }
    @DeleteMapping("/penjualan/{id}")
    public ResponseEntity<?> delPenjualan(@PathVariable Long id){
        return penjualanRepo.findById(id)
                .map(penjualan -> {
                    penjualanRepo.delete(penjualan);
                    return ResponseEntity.ok().build();
                }).orElseThrow(()->new DataNotFoundException(id));
    }
}
