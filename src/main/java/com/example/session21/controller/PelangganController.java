package com.example.session21.controller;

import com.example.session21.exception.DataNotFoundException;
import com.example.session21.model.Pelanggan;
import com.example.session21.repository.PelangganRepo;
import com.example.session21.repository.PenjualanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PelangganController {
    @Autowired
    PelangganRepo pelangganRepo;

    @GetMapping("/pelanggan/all")
    List<Pelanggan> getPelanggan(){
        return pelangganRepo.findAll();
    }
    @GetMapping("/pelanggan")
    List<Pelanggan> getPelanggan(String nama){
        if (pelangganRepo.findByNama(nama).isEmpty()){
            throw new DataNotFoundException(nama);
        }
        return pelangganRepo.findByNama(nama);
    }
    @PostMapping("/pelanggan")
    public Pelanggan newPelanggan(@Valid @RequestBody Pelanggan pelanggan){
        return pelangganRepo.save(pelanggan);
    }
    @PutMapping("/pelanggan/{id}")
    public Pelanggan editPelanggan(@PathVariable Long id,
                                   @Valid @RequestBody Pelanggan pelangganReq){
        return pelangganRepo.findById(id)
                .map(pelanggan -> {
                    pelanggan.setNama(pelangganReq.getNama());
                    pelanggan.setAlamat(pelangganReq.getAlamat());
                    return pelangganRepo.save(pelanggan);
                }).orElseThrow(()->new DataNotFoundException(id));
    }
    @DeleteMapping("/pelanggan/{id}")
    public ResponseEntity<?> delPelanggan(@PathVariable Long id){
        return pelangganRepo.findById(id)
                .map(pelanggan -> {
                    pelangganRepo.delete(pelanggan);
                    return ResponseEntity.ok().build();
                }).orElseThrow(()->new DataNotFoundException(id));
    }
}
