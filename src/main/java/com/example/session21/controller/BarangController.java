package com.example.session21.controller;

import com.example.session21.dto.BarangDTO;
import com.example.session21.exception.DataNotFoundException;
import com.example.session21.model.Barang;
import com.example.session21.repository.BarangRepo;
import com.example.session21.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
public class BarangController {
    @Autowired
    private BarangRepo barangRepo;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @GetMapping("/barang/semua")
    public List<BarangDTO> getBarang() {
        List<Barang> barangList=barangRepo.findAll();
        List<BarangDTO> barangDTOs=new ArrayList<>();
        for(Barang barangData: barangList){
            BarangDTO barangDTO=modelMapperUtil
                    .ModelMapperInit()
                    .map(barangData,BarangDTO.class);
            barangDTO.setId(barangData.getId());
            barangDTO.setNama(barangData.getNama());
            barangDTO.setAsal(barangData.getAsal());
            barangDTOs.add(barangDTO);
        }
        return barangDTOs;
    }

    //pakai GET query params /barang?nama=<nama>
    @GetMapping("/barang")
    public List<Barang> getBarang(String nama){
        if(barangRepo.findByNama(nama).isEmpty()){
            throw new DataNotFoundException(nama);
        }
        return barangRepo.findByNama(nama);
    }

    @PostMapping("/barang")
    public BarangDTO newBarang(@Valid @RequestBody BarangDTO barang) {
       Barang newBarang=modelMapperUtil
               .ModelMapperInit()
               .map(barang,Barang.class);
       newBarang.setNama(barang.getNama());
       newBarang.setAsal(barang.getAsal());
       newBarang.setDeskripsi(barang.getNama());
       barangRepo.save(newBarang);
       return barang;
    }

    @PutMapping("/barang/{id}")
    public Barang editBarang(@PathVariable Long id,
                             @Valid @RequestBody BarangDTO barangReq) {
        Barang findById=barangRepo.findById(id)
                .orElseThrow(()->new DataNotFoundException(id));
        Barang editBarang=modelMapperUtil
                .ModelMapperInit()
                .map(barangReq,Barang.class);
        findById.setNama(barangReq.getNama());
        findById.setAsal(barangReq.getAsal());

        barangRepo.save(findById);
        return editBarang;
    }

    @DeleteMapping("barang/{id}")
    public ResponseEntity<?> delBarang(@PathVariable Long id){
        return barangRepo.findById(id)
                .map(barang -> {
                    barangRepo.delete(barang);
                    return ResponseEntity.ok().build();
                }).orElseThrow(()->new DataNotFoundException(id));
    }
}
