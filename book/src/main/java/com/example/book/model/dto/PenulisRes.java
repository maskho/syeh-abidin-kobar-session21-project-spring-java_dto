package com.example.book.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class PenulisRes {
    private String nama;
    private String judul;

    public PenulisRes(String nama, String judul) {
        this.nama = nama;
        this.judul = judul;
    }
}
