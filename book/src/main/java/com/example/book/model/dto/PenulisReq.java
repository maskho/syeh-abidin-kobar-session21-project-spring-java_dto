package com.example.book.model.dto;

import com.example.book.model.Penulis;
import lombok.Data;

@Data
public class PenulisReq {
    private Penulis penulis;
}
