package com.example.book.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "penulis")
@Data
public class Penulis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column
    private String nama;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Buku.class)
    @JoinColumn(name = "pb_fkey", referencedColumnName = "id")
    private List<Buku> bukus;

}
