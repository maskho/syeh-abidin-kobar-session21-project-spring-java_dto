package com.example.book.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "buku")
@Data
public class Buku {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String judul;
    
//    @ManyToOne
//    @JoinColumn
//    private Penulis penulis;

}
