package com.example.book.controller;

import com.example.book.model.Buku;
import com.example.book.repository.BukuRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BukuController {
    @Autowired
    private BukuRepo bukuRepo;
    
    @GetMapping("/buku/all")
    public List<Buku> getAllBuku(){
        return bukuRepo.findAll();
    }
    @PostMapping("/buku")
    public Buku newBuku(@RequestBody Buku buku){
        return bukuRepo.save(buku);
    }
}
