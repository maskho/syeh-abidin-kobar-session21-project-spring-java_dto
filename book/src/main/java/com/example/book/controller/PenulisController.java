package com.example.book.controller;

import com.example.book.model.Penulis;
import com.example.book.model.dto.PenulisReq;
import com.example.book.model.dto.PenulisRes;
import com.example.book.repository.BukuRepo;
import com.example.book.repository.PenulisRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PenulisController {
    @Autowired
    private PenulisRepo penulisRepo;
    @Autowired
    private BukuRepo bukuRepo;

    @GetMapping("/penulis/all")
    public List<Penulis> getAllPenulis() {
        return penulisRepo.findAll();
    }
    @GetMapping("/penulis/info")
    public List<PenulisRes> getJoinInfo(){
        return penulisRepo.getJoinInfo();
    }

    @PostMapping("/penulis")
    public Penulis newPenulis(@RequestBody PenulisReq req){
        return penulisRepo.save(req.getPenulis());
    }

}
