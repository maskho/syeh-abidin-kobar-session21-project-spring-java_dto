package com.example.book.repository;

import com.example.book.model.Penulis;
import com.example.book.model.dto.PenulisRes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PenulisRepo extends JpaRepository<Penulis, Long> {
    @Query("select new com.example.book.model.dto.PenulisRes(c.nama, p.judul) from Penulis c join c.bukus p")
    public List<PenulisRes> getJoinInfo();
}
